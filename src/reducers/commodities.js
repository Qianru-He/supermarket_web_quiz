import {GET_COMMODITIES} from "../constant/constant";

const initState = {
    commodities:[]
};

export default (state = initState, action) => {
    switch (action.type) {
        case GET_COMMODITIES:
            return {
                ...state,
                commodities:action.payload
            }

        default:
            return state;
    }
}