import {GET_ORDERS} from "../constant/constant";

const initState = {
    orders:[]
};
export default (state = initState, action) => {
    switch (action.type) {
        case GET_ORDERS:
            return {
                ...state,
                orders:action.payload
            };
        default:
            return state;
    }
}