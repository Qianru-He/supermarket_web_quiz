import React, {Component} from 'react';
import {connect} from "react-redux";
import {getCommodities} from "../../actions/commodityAction";
import './commodities.less';
import {Commodity} from "./Commodity";

class Commodities extends Component {
    componentDidMount() {
        this.props.getCommodities();
    }

    render() {
        const commodities = this.props.commodities;
        return (
            <section className="commodity-list">
                <div className='commodity'>
                    {
                        commodities.map((commodity) => {
                            return <Commodity commodity={commodity}/>
                        })
                    }
                </div>
            </section>
        );
    }
}

const mapStateToProps = (state) => {
    console.log(state.commodities.commodities);
    return { commodities:state.commodities.commodities}
};
const mapDispatchToProps = (dispatch) => ({
    getCommodities: () => dispatch(getCommodities())
});
export default connect(mapStateToProps, mapDispatchToProps)(Commodities);