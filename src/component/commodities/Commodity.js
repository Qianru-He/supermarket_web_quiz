import React from 'react'
import './commodity.less';
import {MdAddCircleOutline} from "react-icons/md";

export const Commodity = (props) => {
    const {name, price, unit, image} = props.commodity;
    return <section className='commodity-box'>
        <img src={image}/>
        <article className='commodity-describe'>
            <h1>{name}</h1>
            <span>单价：{price}元/{unit}</span>
            <span className='commodity-add'><MdAddCircleOutline/></span>
        </article>
    </section>
};
