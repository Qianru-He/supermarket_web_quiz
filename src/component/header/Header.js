import React from 'react'
import {Link} from "react-router-dom";
import './header.less';
import {MdAdd, MdAddShoppingCart, MdHome} from "react-icons/md";

const Header = () => {
    return (
        <header className='header'>
            <Link className='link link-check' to='/' ><MdHome/>商城</Link>
            <Link className='link' to='/orders' ><MdAddShoppingCart/>订单</Link>
            <Link className='link' to='/create'><MdAdd/>添加商品</Link>
        </header>
    )
}

export default Header