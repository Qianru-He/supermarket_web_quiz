import React from 'react'

const Footer = () => {
    return (
        <section className='footer'>
            <span>TW Mall @2018 Created by ForCheng</span>
        </section>
    )
}

export default Footer