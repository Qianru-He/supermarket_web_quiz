import React, {Component} from 'react';
import {connect} from "react-redux";
import {createCommodity} from "../../actions/commodityAction";
import './createCommodity.less';

class CommodityCreate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            price: '',
            unit: '',
            image: ''
        }
    }

    handleName(event) {
        this.setState({name: event.currentTarget.value})
    }
    handlePrice(event) {
        this.setState({price: event.currentTarget.value})
    }
    handleUnit(event) {
        this.setState({unit: event.currentTarget.value})
    }
    handleImage(event) {
        this.setState({image: event.currentTarget.value})
    }
    handleSubmit() {
        const {name, price, unit, image} = this.state;
        const data = {name, price, unit, image};
        this.props.createCommodity(data, () => {
            this.props.history.push('/')
        })
    }

    render() {
        const {name, price, unit, image} = this.state;
        return (
            <section className="commodity-create">
                <label>名称
                    <input value={name} onChange={this.handleName.bind(this)} placeholder='名称'/>
                </label>
                <label>价格
                    <input value={price} onChange={this.handlePrice.bind(this)} type='number' placeholder='价格'/>
                </label>
                <label>单位
                    <input value={unit} onChange={this.handleUnit.bind(this)} placeholder='单位'/>
                </label>
                <label>图片
                    <input value={image} onChange={this.handleImage.bind(this)} placeholder='图片'/>
                </label>
                <label>名称
                    <button onClick={this.handleSubmit.bind(this)}>提交</button>
                </label>
            </section>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    createCommodity: (data, callback) => dispatch(createCommodity(data, callback))
});
export default connect(null, mapDispatchToProps)(CommodityCreate);