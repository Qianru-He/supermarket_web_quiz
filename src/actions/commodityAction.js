import {GET_COMMODITIES} from "../constant/constant";

export const getCommodities = () =>(dispatch)=> {
    fetch('http://localhost:8081/api/commodities')
        .then(response => response.json())
        .then((data) => {
            dispatch({
                type: GET_COMMODITIES,
                payload: data
            })
        })
};

export const createCommodity = (data, callback) => (dispatch) => {
    fetch(`http://localhost:8080/api/commodities`,{
        method:'post',
        headers: {'Content-Type': 'application/json;charset=UTF-8'},
        body: JSON.stringify(data)
    })
        .then(() => {
            dispatch(getCommodities());
            callback();
        })
};