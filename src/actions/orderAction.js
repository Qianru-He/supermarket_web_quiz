import {GET_ORDERS} from "../constant/constant";

export const getOrders = () => (dispatch) => {
    fetch('http://localhost:8080/api/orders')
        .then(response => response.json())
        .then((data) => {
            dispatch({
                type: GET_ORDERS,
                payload: data
            })
        })
};
export const deleteOrderByCommodityId = (id) => (dispatch) => {
    fetch(`http://localhost:8080/api/orders/commodities/${id}`, {method: 'delete'})
        .then((data) => {
            dispatch(
                getOrders()
            )
        })
};