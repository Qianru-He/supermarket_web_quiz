import React, { Component } from 'react';
import {BrowserRouter as Router} from 'react-router-dom'
import './App.less';
import Header from "./component/header/Header";
import {Route, Switch} from "react-router";
import Commodities from './component/commodities/Commodities';
import CreateCommodity from "./component/createCommodity/CreateCommodity";
import Order from "./component/order/Order";
import Footer from "./component/footer/Footer";

class App extends Component {
  render() {
    return (
      <Router>
        <Header/>
        <Switch>
          <Route exact path='/' component={Commodities}/>
          <Route path='/create' component={CreateCommodity}/>
          <Route path='/orders' component={Order}/>
        </Switch>
          <Footer/>
      </Router>
    );
  }
}

export default App;
